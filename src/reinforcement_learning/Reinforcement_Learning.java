/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package reinforcement_learning;
import java.text.DecimalFormat;
import java.util.Random;

/**
 *
 * @author Nisarg
 */
public class Reinforcement_Learning {

    
    
    /**
     * @param args the command line arguments
     */
    int rowSize = 5;
    int colSize = 6;
    
    
    public enum direction {W, N, E, S};
    
    public static void main(String[] args) {
        //Initialize arrays, row/col, direction, tempDirection
        State[][] N = new State[5][6];
        State[][] Q = new State[5][6];
        RowCol curr = new RowCol();
        direction tempDirection;
        
        //Setup tables
        setupN(N);
        setupQ(Q);
          
        
        
        //Trials
//        curr = getState(N);
//        curr.row = 3;
//        curr.col = 5;
//        if (!N[curr.row][curr.col].goalState)
//        {
//            System.out.println("Row = " + Double.toString(curr.row) + ", Col = " + Double.toString(curr.col));
//            
//            tempDirection = getAction(Q, curr);
////            tempDirection = direction.N;
//            System.out.println("Direction selected: " + tempDirection);
//            
//            incrementN(N, curr, tempDirection);
//            
//            updateQ(Q, N, curr, tempDirection);
//            
//            tempDirection = simulateOutcome(N, curr, tempDirection);
//            System.out.println("Actual Direction Moved: " + tempDirection);
//            
//            move(N, curr, tempDirection);
//            System.out.println("New Row: " + curr.row + ", New Col: " + curr.col);
//        }
        
        
        
        
        
        
        
        
        
        
        //iteration of 10,000 trials
        int numTrials = 50;
        for (int i = 0; i < numTrials; i++)
        {
            curr = getState(N);    //Randomly select a state
            //while not reached the goal state, keep looping
            while(!N[curr.row][curr.col].goalState)
            {
                //Decide on an action using Q-table
                tempDirection = getAction(Q, curr);
                
                //Increment N(s,a)
                incrementN(N, curr, tempDirection);
                
                //Update Q(s,a)    
                updateQ(Q, N, curr, tempDirection);
                
                //Simulate actual outcome
                tempDirection = simulateOutcome(N, curr, tempDirection);
                
                
                //Move there and update row/col
                move(N, curr, tempDirection);
                
                
                
            }
        }
        
        //Print N table, Q table, and Optimal Action table
        System.out.println("Table of N(s,a):");
        printN(N);
        System.out.println();
        System.out.println("Table of Q(s,a):");
        printQ(Q);
        System.out.println();
        printOptimalAction(N, Q);
        
        
        
        
    }
    
    
    
    //Function to set up matrix for N
    public static void setupN(State[][] N)
    {
      for (int row = 0; row < 5; row++)
      {
          for (int col = 0; col < 6; col++)
          {
              N[row][col] = new State();
              N[row][col].west = 0.0;
              N[row][col].north = 0.0;
              N[row][col].east = 0.0;
              N[row][col].south = 0.0;
          }
      }
      
      N[2][3].goalState = true;
      N[1][1].boundary = true;
      N[2][1].boundary = true;
      N[3][1].boundary = true;
      N[1][2].boundary = true;
      N[1][3].boundary = true;
      N[1][4].boundary = true;
      N[2][4].boundary = true;
      
    }
    
    //Function to set up matrix for Q
    public static void setupQ(State[][] Q)
    {
        
      for (int row = 0; row < 5; row++)
      {
          for (int col = 0; col < 6; col++)
          {
              Q[row][col] = new State();
              Q[row][col].west = 0.0;
              Q[row][col].north = 0.0;
              Q[row][col].east = 0.0;
              Q[row][col].south = 0.0;
          }
      }
      
      Q[2][3].goalState = true;
      Q[2][3].west = 50.0;
      Q[2][3].north = 50.0;
      Q[2][3].east = 50.0;
      Q[2][3].south = 50.0;
      Q[1][1].boundary = true;
      Q[2][1].boundary = true;
      Q[3][1].boundary = true;
      Q[1][2].boundary = true;
      Q[1][3].boundary = true;
      Q[1][4].boundary = true;
      Q[2][4].boundary = true;
    }
    
    //Function to print N table
    public static void printN(State[][] N)
    {
        DecimalFormat df = new DecimalFormat("####0.0");
        for (int row = 0; row < 5; row++)       //row
        {
            
            //top
            for (int col = 0; col < 6; col++)
            {
                if (N[row][col].boundary)
                {
                    System.out.print("\t\t\t");
                }
                else if (N[row][col].goalState)
                {
                    System.out.print("\t\t\t");
                }
                else {
                    System.out.print("\t" + df.format(Double.toString(N[row][col].north)) + "\t\t");
                }
            }
            System.out.println();
            
            
            //middle
            for (int col = 0; col < 6; col++)
            {
                if (N[row][col].boundary)
                {
                    System.out.print("\t####\t\t");
                }
                else if (N[row][col].goalState)
                {
                    System.out.print("\t+50\t\t");
                }
                else {
                    System.out.print(df.format(Double.toString(N[row][col].west)) + "\t\t" + df.format(Double.toString(N[row][col].east)) + "\t");
                }
            }
            System.out.println();
            
            //bottom
            for (int col = 0; col < 6; col++)
            {
                if (N[row][col].boundary)
                {
                    System.out.print("\t\t\t");
                }
                else if (N[row][col].goalState)
                {
                    System.out.print("\t\t\t");
                }
                else {
                    System.out.print("\t" + df.format(Double.toString(N[row][col].south)) + "\t\t");
                }
            }
            System.out.println();
            System.out.println();
            
        }
    }
    
    //Function to print Q table
    public static void printQ(State[][] Q)
    {
        DecimalFormat df = new DecimalFormat("####0.0");
        for (int row = 0; row < 5; row++)       //row
        {
            
            //top
            for (int col = 0; col < 6; col++)
            {
                if (Q[row][col].boundary)
                {
                    System.out.print("\t\t\t");
                }
                else if (Q[row][col].goalState)
                {
                    System.out.print("\t\t\t");
                }
                else {
                    System.out.print("\t" + df.format(Double.toString(Q[row][col].north)) + "\t\t");
                }
            }
            System.out.println();
            
            
            //middle
            for (int col = 0; col < 6; col++)
            {
                if (Q[row][col].boundary)
                {
                    System.out.print("\t####\t\t");
                }
                else if (Q[row][col].goalState)
                {
                    System.out.print("\t+50\t\t");
                }
                else {
                    System.out.print(df.format(Double.toString(Q[row][col].west)) + "\t\t" + df.format(Double.toString(Q[row][col].east)) + "\t");
                }
            }
            System.out.println();
            
            //bottom
            for (int col = 0; col < 6; col++)
            {
                if (Q[row][col].boundary)
                {
                    System.out.print("\t\t\t");
                }
                else if (Q[row][col].goalState)
                {
                    System.out.print("\t\t\t");
                }
                else {
                    System.out.print("\t" + df.format(Double.toString(Q[row][col].south)) + "\t\t");
                }
            }
            System.out.println();
            System.out.println();
            
        }
    }
    
    //Function to print Optimal Action Table
    public static void  printOptimalAction(State[][] N, State[][] Q)
    {
        
    }
    
    //Simulator that decides on an optimal action
    public static direction getAction(State[][] Q, RowCol temp)
    {
        Random rand = new Random();
        Double west = Q[temp.row][temp.col].west;
        Double north = Q[temp.row][temp.col].north;
        Double east = Q[temp.row][temp.col].east;
        Double south = Q[temp.row][temp.col].south;
        
        int choice = rand.nextInt(100);
        if (choice < 95)
        {
            //Find the maximum value
            if (west > north && west > east && west > south)
            {
                return direction.W;
            }
            else if (north > west && north > east && north > south)
            {
                return direction.N;
            }
            else if (east > west && east > north && east > south)
            {
                return direction.E;
            }
            else if (south > west && south > north && south > east)
            {
                return direction.S;
            }
            else {
                //If value acquired is 0 or all same, return random Direction
                int value = rand.nextInt(4);
                switch (value) {
                    case 0:
                        return direction.W;
                    case 1:
                        return direction.N;
                    case 2:
                        return direction.E;
                    default:
                        return direction.S;
                }
            }
        } 
        else {
            int value = rand.nextInt(4);
                switch (value) {
                    case 0:
                        return direction.W;
                    case 1:
                        return direction.N;
                    case 2:
                        return direction.E;
                    default:
                        return direction.S;
                }
        }
    }
    
    public static RowCol getState(State[][] N)
    {
        RowCol temp = new RowCol();
        Random rand = new Random();
        int row; 
        int col;
        do{
            temp.row = rand.nextInt(5);
            temp.col = rand.nextInt(6);
        }while (N[temp.row][temp.col].boundary);
        
        return temp;
    }
    
    
    //Increment N(s,a)
    public static void incrementN(State[][] N, RowCol rowcol, direction A)
    {
        RowCol temp = new RowCol();
        temp.row = rowcol.row;
        temp.col = rowcol.col;
        if (A == direction.W)
        {
            N[temp.row][temp.col].west = N[temp.row][temp.col].west + 1.0;
        }
        else if (A == direction.N)
        {
            N[temp.row][temp.col].north = N[temp.row][temp.col].north + 1.0;
        }
        else if (A == direction.E)
        {
            N[temp.row][temp.col].east = N[temp.row][temp.col].east + 1.0;
        }
        else if (A == direction.S)
        {
            N[temp.row][temp.col].south = N[temp.row][temp.col].south + 1.0;
        }
    }
    
    
    //Update Q(s,a)
    public static void updateQ(State[][] Q, State[][] N, RowCol rowcol, direction A)
    {
        RowCol temp = new RowCol();
        temp.row = rowcol.row;
        temp.col = rowcol.col;
        double y = 0.9;
        double q;
        double r;
        double n;
        double q_prime = highestQ_prime(Q, temp, A);
        
        if (A == direction.W)
        {
            q = Q[temp.row][temp.col].west;
            n = N[temp.row][temp.col].west;
            r = -1.0;
            Q[temp.row][temp.col].west = q + ((1 / n)*(r + (y * q_prime) - q));
        }
        else if (A == direction.N)
        {
            q = Q[temp.row][temp.col].north;
            n = N[temp.row][temp.col].north;
            r = -2.0;
            Q[temp.row][temp.col].north = q + ((1 / n)*(r + (y * q_prime) - q));
        }
        else if (A == direction.E)
        {
            q = Q[temp.row][temp.col].east;
            n = N[temp.row][temp.col].east;
            r = -3.0;
            Q[temp.row][temp.col].east = q + ((1 / n)*(r + (y * q_prime) - q));
        }
        else if (A == direction.S) {
            q = Q[temp.row][temp.col].south;
            n = N[temp.row][temp.col].south;
            r = -2.0;
            Q[temp.row][temp.col].south = q + ((1 / n)*(r + (y * q_prime) - q));
        }
    }
    
    
    //Simulator gives the actual outcome that the Angent has to perform
    public static direction simulateOutcome(State[][] N, RowCol temp, direction A)
    {
        Random rand = new Random();
        int value = rand.nextInt(100);
        
        if (value < 10)
        {
            if (A == direction.W)
            {
                return direction.S;
            }
            else if (A == direction. N)
            {
                return direction.W;
            }
            else if (A == direction.E)
            {
                return direction.N;
            }
            else
            {
                return direction.E;
            }
        }
        else if (value < 20)
        {
            if (A == direction.W)
            {
                return direction.N;
            }
            else if (A == direction. N)
            {
                return direction.E;
            }
            else if (A == direction.E)
            {
                return direction.S;
            }
            else
            {
                return direction.W;
            }
        }
        else {
            return A;
        }
    }
    
    
    //Move current row / column in the given direction
    public static void move(State[][] N, RowCol temp, direction A)
    {
        if (A == direction.W)
        {
            if (temp.col-1 > 0 && !N[temp.row][temp.col-1].boundary)
            {
                temp.col = temp.col - 1;
            }
        }
        else if (A == direction.N)
        {
            if (temp.row-1 > 0 && !N[temp.row-1][temp.col].boundary)
            {
                temp.row = temp.row - 1;
            }
        }
        else if (A == direction.E)
        {
            if (temp.col+1 <= 5 && !N[temp.row][temp.col+1].boundary)
            {
                temp.col = temp.col + 1;
            }
        }
        else if (A == direction.S) 
        {
            if (temp.row+1 <= 4 && !N[temp.row+1][temp.col].boundary)
            {
                temp.row = temp.row + 1;
            }
        }
    }
    
    
    
    //Find the highest Q
    public static double highestQ(State[][] Q, RowCol rowcol)
    {
        RowCol temp = new RowCol();
        temp.row = rowcol.row;
        temp.col = rowcol.col;
        Random rand = new Random();
        Double west = Q[temp.row][temp.col].west;
        Double north = Q[temp.row][temp.col].north;
        Double east = Q[temp.row][temp.col].east;
        Double south = Q[temp.row][temp.col].south;
        if (west > north && west > east && west > south)
        {
            return Q[temp.row][temp.col].west;
        }
        else if (north > west && north > east && north > south)
        {
            return Q[temp.row][temp.col].north;
        }
        else if (east > west && east > north && east > south)
        {
            return Q[temp.row][temp.col].east;
        }
        else if (south > west && south > north && south > east)
        {
            return Q[temp.row][temp.col].south;
        }
        else {
            return Q[temp.row][temp.col].west;
        }
    }
    
    
    
    public static double highestQ_prime(State[][] Q, RowCol rowcol, direction A)
    {
        RowCol temp = new RowCol();
        temp.row = rowcol.row;
        temp.col = rowcol.col;
        if (A == direction.W && rowcol.col-1 >= 0)
        {
            if (Q[rowcol.row][rowcol.col-1].boundary)
            {
                return Q[temp.row][temp.col].west;
            }
            else {
                temp.col = temp.col - 1;
                return highestQ(Q, temp);
            }
        }
        else if (A == direction.N && rowcol.row-1 >= 0)
        {
            if (Q[rowcol.row-1][rowcol.col].boundary)
            {
                return Q[temp.row][temp.col].north;
            }
            else {
                temp.row = temp.row - 1;
                return highestQ(Q, temp);
            }
        }
        else if (A == direction.E && rowcol.col+1 <= 5)
        {
            if (Q[rowcol.row][rowcol.col+1].boundary)
            {
                return Q[temp.row][temp.col].east;
            }
            else {
                temp.col = temp.col + 1;
                return highestQ(Q, temp);
            }
        }
        else if (A == direction. S && rowcol.row+1 <= 4)
        {
            if (Q[rowcol.row+1][rowcol.col].boundary)
            {
                return Q[temp.row][temp.col].south;
            }
            else {
                temp.row = temp.row + 1;
                return highestQ(Q, temp);
            }
        }
        else {
            return -2.0;
        }
    }
    
    
}

